Meteor.methods({
	addOwner: function(sessionId){
		var uploads = Uploads.find({},{sessionId: sessionId});
		var userId = Meteor.userId();

		uploads.forEach(
			function (upload) {
				if(upload.owner == null || upload.owner == "undefined"){
					Uploads.update(upload._id, {
						$set: {
							'owner': userId
						}
					});
				}
			}
		);
	},

	sendEmail: function(){

		var user = Meteor.user();
		var email;

		if(user.services.google != null){
			email = user.services.google.email;
		}

		if(user.services.facebook != null) {
			email = user.services.facebook.email;
		}
		if(user.emails != null){
			email = user.emails[0].address;
		}


		console.log("email: " + email);


//		Email.send({
//			to: "shanebenjamin.sb@gmail.com",
//			from: "transcode@email.com",
//			subject: "Example Email",
//			text: "The contents of our email in plain text."
//		});

	},

	deleteUpload: function(uploadId){
		var upload = Uploads.findOne(uploadId);
		var user = Meteor.user();

		if(upload.owner != user._id){
			throw new Meteor.Error("402", "You are not the owner of this file.");
		}

		Uploads.remove(upload._id);

		var fs = Npm.require("fs");

		var route = "/.uploads/" + upload.type + "/";


		fs.readdir(process.env.PWD + route, function(err, files) {
			if (err) {
				console.log(err);
			}

			fs.unlink(process.env.PWD + route + upload.fileName);

		});
	},

	convertUpload: function(uploadId){
		var dir, email, outPut, exec, cmd;
		var user = Meteor.user();
		var fs = Npm.require("fs");
		var upload = Uploads.findOne(uploadId);
		var path = process.env.PWD;
		var route = path + "/.uploads/" + upload.type + "/";

		exec = Npm.require('child_process').exec;
		cmd = Meteor.wrapAsync(exec);

		console.log("convert to type: " + upload.convertTo);

		if(upload.type == "audio"){
			outPut = upload.fileName.toString().replace(/.mp(?:3|4)/i, "." + upload.convertTo);
			console.log("I'm an audio");
		}
		else {
			outPut = upload.fileName.toString().replace(/.mp(?:3|4)/i, "." + upload.convertTo);
			console.log("I'm a video");
		}

		dir = path + "/public/.converted/" + upload.type + "/";

		console.log("out put: " + outPut);
		console.log("title: " + upload.title);

		function checkDirectory(directory, callback) {
			fs.stat(directory, function(err, stats) {
				//Check if error defined and the error code is "not exists"
				if (err && err.errno === 34) {
					//Create the directory, call the callback.
					fs.mkdir(directory, callback);
				} else {
					//just in case there was a different error:
//					callback(err)
				}
			});
		}

		checkDirectory(dir);


		cmd("ffmpeg -i " + route + upload.fileName + " -acodec copy -vcodec copy " + dir + outPut);

		Uploads.update(upload._id, {
			$set: {
				'convertedFileName': outPut
			}
		});

		if(user.services.google != null){
			email = user.services.google.email;
		}

		if(user.services.facebook != null) {
			email = user.services.facebook.email;
		}
		if(user.emails != null){
			email = user.emails[0].address;
		}

		Email.send({
			to: email,
			from: "transcode@email.com",
			subject: "Download read",
			text: 'Your download is read. Click here http://localhost:3000/ to go to download.'
		});

	},

	editAnUpload: function(uploadItem){
		var upload = Uploads.findOne(uploadItem.uploadId);

		console.log("test: ", uploadItem.type);

		if(upload.convertTo == null || upload.convertTo == "undefined"){
			if(upload.type == "video"){
				if(uploadItem.type == "avi" || uploadItem.type == "mkv" || uploadItem.type == "mov"){
					Uploads.update(upload._id, {
						$set: {
							convertTo: uploadItem.type
						}
					});
				}
				else {
					throw new Meteor.Error("404", "Please select one of the given file types");
				}
			}
			else{
				if(uploadItem.type == "ogg" || uploadItem.type == "wav" || uploadItem.type == "wma"){
					Uploads.update(upload._id, {
						$set: {
							convertTo: uploadItem.type
						}
					});
				}
				else {
					throw new Meteor.Error("404", "Please select one of the given file types");
				}
			}
		}
	}

});