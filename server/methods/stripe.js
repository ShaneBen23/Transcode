Meteor.methods({
	charge: function (token, clientUpload) {
		this.unblock();
		check(token, String);

		var upload = Uploads.findOne(clientUpload._id);

		if(!upload){
			throw new Meteor.Error('404', 'Upload does not exist!');
		}

		try {
			var Stripe = StripeAPI('sk_test_Gbkcd1vyysglMwdVo3zlWN1b');

			var result = Stripe.charges.create({
				amount: parseFloat(upload.price).toFixed(2) * 100,
				currency: 'eur',
				source: token,
				description: 'Convert and download ' + upload.title
			});

			Uploads.update(upload._id,{
				$set:{
					paid: true
				}
			});

			// do something with result, save to db maybe?

			return true;
		}
		catch(ex){
			throw new Meteor.Error('payment-failed', 'The payment failed');
		}
	},

	chargeExtend: function (token) {
		this.unblock();
		check(token, String);

		try {
			var Stripe = StripeAPI('sk_test_Gbkcd1vyysglMwdVo3zlWN1b');

			var result = Stripe.charges.create({
				amount: 1000,
				currency: 'eur',
				source: token,
				description: 'User expand upload limit'
			});

			var user = Meteor.user();

			var newLimit;

			if(user.profile.uploadLimit != null){
				newLimit = user.profile.uploadLimit + 5;
			}
			else {
				newLimit = 15;
			}


			Meteor.users.update(user._id, {
				$set: {
					'profile.uploadLimit': newLimit
				}
			});

			console.log("Alllllll is well");

			// do something with result, save to db maybe?

			return true;
		}
		catch(ex){
			throw new Meteor.Error('payment-failed', 'The payment failed');
		}
	}
});
