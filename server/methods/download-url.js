//import download from "download-file";

Meteor.methods({

	downloadUrl: (file) => {
		var download = require('download-file');

//		var reg = /.mp(?:3|4)/i;
		var regMp3 = /.mp3/i;
		var regMp4 = /.mp4/i;
//		console.log(url);

		if(file.url == null || file.url == ''){
			throw new Meteor.Error("404", "Please enter a valid url.");
		}

		if(file.title == null || file.title == ''){
			throw new Meteor.Error("404", "Please add a title.");
		}

		var urlEnd = file.url.substr(file.url.length - 4);

		var upload, options;
		var uploadedAt = new Date().getTime();



//	mp4 test link
//      http://www.webestools.com/page/media/videoTag/BigBuckBunny.mp4

//  mp3 test link
//		http://sound17.mp3slash.net/320/indian/sanamre/01%20-%20Sanam%20Re%20(Title%20Song)%20[Songspk.LINK].mp3
//		http://www.songspk.live/mp3/indian_songs/sanam_re_mp3_songs



		if(regMp4.test(urlEnd)){

			options = {
				directory: process.env.PWD + '/.uploads/video',
//				directory: process.env.PWD + '/.uploads/',
				filename: "video_" + uploadedAt + ".mp4"
			};

			upload ={
				title: file.title + ".mp4",
				fileName: options.filename,
				type: 'video',
				uploadedAt: uploadedAt,
				sessionId: file.sessionId,
				convertTo: null,
				paid: false
			};

			console.log(urlEnd);

		}

		if(regMp3.test(urlEnd)){

			options = {
				directory: process.env.PWD + '/.uploads/audio',
//				directory: process.env.PWD + '/.uploads/',
				filename:"audio_" + uploadedAt + ".mp3"
			};

			upload ={
				title: file.title + ".mp3",
				fileName: options.filename,
				type: 'audio',
				uploadedAt: uploadedAt,
				sessionId: file.sessionId,
				convertTo: null,
				paid: false
			};

			console.log(urlEnd);
		}

		if(!regMp3.test(urlEnd) && !regMp4.test(urlEnd)) {
			throw new Meteor.Error("404", "Must be a mp3 or mp4.");
		}

		download(file.url, options, function(err){
			if (err){
				throw err;
			}
		});

		Meteor.setTimeout(function () {
			Meteor.call('getFileSize', upload);
		}, 2 * 1000);
	},


	getFileSize: (upload) => {

		var fs = Npm.require("fs");

//		var route = process.env.PWD + "/.uploads/";
		var route = process.env.PWD + "/.uploads/" + upload.type + "/";

		fs.stat(route + upload.fileName, function (err, stat) {
			if (err){
				return console.error(err);
			}

			console.log(stat.size);

			upload.sizeByte = stat.size;
			upload.sizeMb = (stat.size/1000000).toFixed(2);

			if(upload.type == "video"){
				upload.price = (upload.sizeMb * 0.35).toFixed(2);
			}

			if(upload.type == "audio"){
				upload.price = (upload.sizeMb * 0.50).toFixed(2);
			}

		});

		Meteor.setTimeout(function () {
			console.log("upload: ", upload);
			Uploads.insert(upload);
		}, 2 * 1000);

	}

});
