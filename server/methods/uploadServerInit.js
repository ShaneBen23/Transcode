Meteor.startup(function () {
	UploadServer.init({
		tmpDir: process.env.PWD + '/.uploads/tmp',
		uploadDir: process.env.PWD + '/.uploads/',
		checkCreateDirectories: true, //create the directories for you
		//acceptFileTypes: /.mp(?:3|4)|wma|ogg|wav|avi|mkv|mov/i,
		acceptFileTypes: /.mp(?:3|4)/i,
		getDirectory: function(fileInfo, formData) {
			// create a sub-directory in the uploadDir based on the content type (e.g. 'images')

			if(fileInfo.type == "audio/mp3"){
				return '/audio/'
			}

			if(fileInfo.type == "video/mp4"){
				return '/video/'
			}

			return '/trash/'
		},
		finished: function(fileInfo, formFields) {
			// perform a disk operation
			var upload ={
				title: fileInfo.originalName,
				fileName: fileInfo.name,
				type: fileInfo.flatType,
				uploadedAt: fileInfo.uploadedAt,
				sizeByte: fileInfo.size,
				sizeMb: (fileInfo.size/1000000).toFixed(2),
				sessionId: fileInfo.sessionId,
				convertTo: null,
				paid: false
			};

			console.log(fileInfo.flatType);
			console.log(upload.type);

			if(fileInfo.type == "video/mp4"){
				upload.price = (upload.sizeMb * 0.03).toFixed(2);
			}

			if(fileInfo.type == "audio/mp3"){
				upload.price = (upload.sizeMb * 0.05).toFixed(2);
			}

			Uploads.insert(upload);
		},
		getFileName: function(fileInfo, formData) {
			fileInfo.originalName = fileInfo.name;
			fileInfo.uploadedAt = new Date().getTime();
			fileInfo.sessionId = formData.sessionId;

			if(fileInfo.type == "audio/mp3"){
				fileInfo.flatType = 'audio';
				return "audio_" + fileInfo.uploadedAt + ".mp3";
			}

			if(fileInfo.type == "video/mp4"){
				fileInfo.flatType = 'video';
				return "video_" + fileInfo.uploadedAt + ".mp4";
			}
		},
		cacheTime: 100,
		mimeTypes: {
			"mp4": "video/mp4",
			"mp3": "audio/mp3"
		}
	});
});


