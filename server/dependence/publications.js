Meteor.publish('uploads', function(sessionId) {
	if(sessionId == null || sessionId == "undefined"){
		return [];
	}
	if(this.userId){
		return Uploads.find({$or: [{owner: this.userId}, {sessionId: sessionId}]});
	}
	return Uploads.find({sessionId: sessionId});
});

Meteor.publish('user', function() {
	return Meteor.users.find(this.userId,
		{
			fields:{
				'emails':true,
				'email':true
			}
		});
});
