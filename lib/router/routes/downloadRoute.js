Router.route('/download/:_id/', {
	name: 'download',
	template: 'download',
	controller: "connectedController",
	waitOn: function() {
		return [
			Meteor.subscribe('uploads', Session.get("connectedAnonymousUser"))
		]
	},
	data: function() {
		var upload = Uploads.findOne(this.params._id);
		var myRouter = this;
		if(upload.paid){
			myRouter.redirect('uploadFile');
		}
		else{
			return Uploads.findOne(this.params._id);
		}
	}
});

