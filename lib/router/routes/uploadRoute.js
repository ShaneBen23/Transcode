Router.route('/', {
	name: 'uploadFile',
	template: 'uploadFile',
	waitOn: function() {
		return [
			Meteor.subscribe('uploads', Session.get("connectedAnonymousUser"))
		];
	}
});
