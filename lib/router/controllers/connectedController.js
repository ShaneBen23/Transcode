connectedController = RouteController.extend({
	onBeforeAction: function () {
		var myRouter = this;
		if (!Meteor.loggingIn()) {
			if (Meteor.userId()) {

				var uploads = Uploads.find({});

				var total = 0;

				uploads.forEach(
						function (upload) {
							total = total + parseFloat(upload.sizeMb);
						}
				);

				var totalInGB = (total/1024).toFixed(2);

				var uploadLimit;

				if(Meteor.user().profile.uploadLimit != null){
					uploadLimit = Meteor.user().profile.uploadLimit;
				}
				else {
					uploadLimit = 10;
				}

				if(totalInGB > uploadLimit){
					swal({
						title: "Storage full",
						text: "Please delete some files to upload new ones.",
						type: "warning",
						showCancelButton: false,
						confirmButtonColor: "#2980b9",
						confirmButtonText: "Go to uploads",
						closeOnConfirm: true
					}, function(){
						myRouter.redirect('uploadFile');
					});
				}else {
					this.next();
				}

			} else {
				swal({
					title: "Continue",
					text: "You must sign in to continue to download",
					type: "info",
					showCancelButton: true,
					confirmButtonColor: "#2980b9",
					confirmButtonText: "Continue to sign in",
					closeOnConfirm: true
				}, function(){
					myRouter.redirect('atSignIn');
				});

			}
		}
	}
});
