Template.uploadItem.helpers({
	color: function(){
		if(this.type == "video"){
			return "warning"
		}
		else {
			return "info"
		}
	},

	convertType: function(){
		if(this.convertTo == null){
			return true
		}
	},

	video: function(){
		return this.type == "video";
	},

	fileSize: function(){

		var fileSize = parseFloat(this.sizeMb).toFixed(2);

		if(fileSize > 1000){
			return (fileSize / 1000).toFixed(2) + ' Gb';
		}

		return fileSize + ' Mb';


	}
});


Template.uploadItem.events({
	'click .delete': function(){
		var uploadId = this._id;
		swal({
			title: "Delete",
			text: "Are you sure you want to delete this Upload",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#2980b9",
			confirmButtonText: "Yes, I am",
			closeOnConfirm: true
		}, function(){
			Meteor.call("deleteUpload", uploadId, function(error){
					if(error){
						swal("Error", error.reason, "error");
					}
			});
		});
	},

	'click .update': function(){
		var upload = this;

		var update ={
			uploadId: upload._id
		};

		if(this.type == "video"){
			update.type= $("#selectorVideo option:selected").attr("id")
		}

		if(this.type == "audio"){
			update.type= $("#selectorAudio option:selected").attr("id")
		}

		Meteor.call("editAnUpload", update, function(error){
			if(error){
				swal("Error", error.reason, "error");
			}
		})
	},

	'click .email': function(){
		Meteor.call("sendEmail", function(error){
			if(error){
				swal("Error", error.reason, "error");
			}
		})
	}


});