Template.uploadFile.helpers({
	uploads: function(){
		return Uploads.find({},{sort: {uploadedAt: -1}});
	},
	noUploads: function(){
		return Uploads.find().count() == 0;
	},
	storageUsed: function(){
		var uploads = Uploads.find({});

		var total = 0;

		uploads.forEach(
				function (upload) {
					total = total + parseFloat(upload.sizeMb);
				}
		);

		var totalInGB = (total/1024).toFixed(2);

		return totalInGB;
	}
});

Template.uploadFile.events({
	'click #uploadUrl': () => {
		var file = {
			url: $('#uploadUrl').val(),
			title: $('#title').val(),
			sessionId: Session.get('connectedAnonymousUser')
		};

		console.log('url: ', file);

		Meteor.call('downloadUrl', file, (error) => {
			if(error){
				swal('Error', error.reason, "error");
			}else {
				$('#uploadUrl').val('');
				$('#title').val('');
			}
		});
	}
});