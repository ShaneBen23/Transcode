Template.download.helpers({
	color: function(){
		if(this.type == "video"){
			return "warning"
		}
		else {
			return "info"
		}
	},

	titleColor: function(){
		if(this.type == "video"){
			return "videoTitle"
		}
		else {
			return "audioColor"
		}
	}
});

Template.download.events({
	'click #pay': function(e){
		e.preventDefault();
		var upload = this;

		if(upload.convertTo == null || upload.convertTo == "undefined"){
			swal("Error", 'error', "error");
		}

		var handler = StripeCheckout.configure({
			key: 'pk_test_xMFwfp5WYU0IuRBhS2G8vaif',
			image: '/img/logo.png',
			token: function(token) {
				Meteor.call('charge', token.id, upload, function (error, result) {
					if(error){

					}else {
						Meteor.call("convertUpload", upload._id, function(error, res){
							if(error){
								swal("Error", error.reason, "error");
							}
						});
					}
				});
			}
		});

		handler.open({
			name: 'Transcode',
			description: 'Convert and download ' + upload.title,
			zipCode: false,
			amount: parseFloat(upload.price).toFixed(2) * 100,
			currency: "eur"
		});
	}
});
