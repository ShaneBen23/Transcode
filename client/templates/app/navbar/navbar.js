

Template.navbar.events({
	'click #readAllNotifications': function(){
		Meteor.call("seeAllNotifications", Meteor.userId());
	},

	'click .logout': function(){
		Session.clear();
		Meteor.logout();
		Router.go('atSignIn');
	},

	'click #expand': function(e) {
		e.preventDefault();
		var handler = StripeCheckout.configure({
			key: 'pk_test_xMFwfp5WYU0IuRBhS2G8vaif',
			image: '/img/logo.png',
			token: function(token) {
				Meteor.call('chargeExtend', token.id, function (error, result) {
					if(error){
						console.log("Error: " + error.message);
					}
				});
			}
		});

		handler.open({
			name: 'Transcode',
			description: 'Expand storage by 5 GB',
			zipCode: false,
			amount: 1000,
			currency: "eur"
		});
	}
});

Template.navbar.helpers({
	hasNotifications: function(){
		return Notifications.find({seen: false}).count() > 0;
	},

	notificationCount:function(){
		return Notifications.find({seen: false}).count();
	},

	checkRoute: function () {
		var routeName = Router.current().route.getName();

		if(routeName == "events" || routeName == "attendedEvent"){
			return true;
		}


	}
});

